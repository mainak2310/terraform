provider "aws" {
  region = "us-east-2"
  access_key = "${var.access_key}"
  secret_key = "${var.secret_key}"
}

resource "aws_instance" "web" {
  ami           = "ami-00bf61217e296b409"
  instance_type = "t2.micro"
}